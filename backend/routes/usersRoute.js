const router = require('express').Router()
var Users = require('../models/usersModel')

router.route('/add').post((req, res) => {
  const name = req.body.name
  const username = req.body.username
  const password = req.body.password
  Users.find({ username: username })
    .then(dt => {
      if (dt.length > 0) {
        res.json('x')
      } else {
        const newUser = new Users({
          name,
          username,
          password
        })
        newUser.save()
          .then(() => res.json('Success'))
      }
    })
})

router.route('/login').post((req, res) => {
  const username = req.body.username
  const password = req.body.password

  Users.find({ username: username })
    .then(dt => {
      if (dt.length > 0) {
        if (dt[0].username === username) {
          if (dt[0].password === password) {
            res.json(dt)
          } else {
            res.json('x')
          }
        } else {
          res.json('x')
        }
      } else {
        res.json('x')
      }
    })
})

module.exports = router
