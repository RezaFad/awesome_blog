const mongoose = require('mongoose')
const Schema = mongoose.Schema

const blogsSchema = new Schema({
  title: {
    type: String,
    require: true
  },
  content: {
    type: String,
    required: true
  },
  publishDate: {
    type: String,
    require: true
  },
  publishTime: {
    type: String,
    required: true
  },
  authors: {
    type: String,
    required: true
  },
  avatars: {
    type: String,
    required: true
  },
  categories: {
    type: Array,
    required: true
  }
})

const Blogs = mongoose.model('Blogs', blogsSchema)
module.exports = Blogs
