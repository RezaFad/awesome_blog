export function addBlogsMut (state, payload) {
  state.blogs.push(payload)
}

export function updateBlogsMut (state, payload) {
  state.blogs.map((dt, i) => {
    if (dt.id.toString() === payload.id.toString()) {
      Object.assign(state.blogs[i], payload)
    }
  })
}

export function addUsersMut (state, payload) {
  state.users.push(payload)
}
