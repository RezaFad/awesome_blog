const router = require('express').Router()
var Blogs = require('../models/blogsModel')

router.route('/').get((req, res) => {
  Blogs.find()
    .then(blogs => res.json(blogs))
    .catch(err => res.status(400).json('Error: ' + err))
})

router.route('/:id').get((req, res) => {
  Blogs.findById(req.params.id)
    .then(blogs => res.json(blogs))
    .catch(err => res.status(400).json('Error: ' + err))
})

router.route('/add').post((req, res) => {
  const title = req.body.title
  const content = req.body.content
  const publishDate = req.body.publishDate
  const publishTime = req.body.publishTime
  const authors = req.body.authors
  const avatars = req.body.avatars
  const categories = req.body.categories
  const newBlogs = new Blogs({
    title,
    content,
    publishDate,
    publishTime,
    authors,
    avatars,
    categories
  })

  newBlogs.save()
    .then(() => res.json('Success Add New Blog'))
    .catch(err => res.status(400).json('Error: ' + err))
})

router.route('/update/:id').post((req, res) => {
  Blogs.findById(req.params.id)
    .then(dt => {
      dt.title = req.body.title
      dt.content = req.body.content
      dt.publishDate = req.body.publishDate
      dt.publishTime = req.body.publishTime
      dt.authors = req.body.authors
      dt.avatars = req.body.avatars
      dt.categories = req.body.categories

      dt.save()
        .then(() => res.json('Success Update Data'))
    })
})

module.exports = router
