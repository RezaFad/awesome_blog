const mongoose = require('mongoose')
const Schema = mongoose.Schema

const usersSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    require: true
  }
})

const Users = mongoose.model('Users', usersSchema)
module.exports = Users
