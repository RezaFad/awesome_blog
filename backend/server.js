const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const app = express()
require('dotenv').config()

// port
const port = process.env.PORT || 5000

// config
app.use((cors()))
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: false }))

// mongoodb config
const uri = process.env.ATLAS_URI
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
mongoose.Promise = global.Promise
const connection = mongoose.connection
connection.once('open', () => {
  console.log('MongoDB database connection established successfully')
})

const blogsRouter = require('./routes/blogsRoute')
const usersRouter = require('./routes/usersRoute')

app.use('/blogs', blogsRouter)
app.use('/users', usersRouter)

app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.json({ error: err })
})

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`)
})
